KCfindall
=========
KC Finder and CKEditor

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist stoat/stoat-kcfindall "*"
```

or add

```
"stoat/stoat-kcfindall": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \stoat\kcfindall\AutoloadExample::widget(); ?>```